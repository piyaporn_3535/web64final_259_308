import logo from './logo.svg';
import './App.css';
import HomePage from './pages/HomePage';
import LoginPage from './pages/LoginPage';
import Header from './components/Header';
import HomelogPage from './pages/HomelogPage';

import {Routes,Route} from "react-router-dom";
import RegisterPage from './pages/RegisterPage';
import RegistrationPage from './pages/RegistrationPage';
import SuccessPage from './pages/SuccessPage'


function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
      <Route path ="" element = {
                <HomePage/>
                } />   
        <Route path ="/log" element = {
                <LoginPage/>
                } />     
        <Route path = "/ris" element = {
                <RegisterPage/>
                } />
        <Route path = "/risT" element = {
                <RegistrationPage/>
                } />
         <Route path = "/Hl" element = {
                <HomelogPage/>
                } />
         <Route path = "/suc" element = {
                <SuccessPage/>
                } />
        </Routes>
    </div>
  );
}

export default App;
