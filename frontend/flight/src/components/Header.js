

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import {useNavigate} from "react-router-dom"
import SvgIcon from '@mui/material/SvgIcon';
import { yellow } from '@mui/material/colors';

function HomeIcon(props) {
  return (
    <SvgIcon {...props}>
      <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
    </SvgIcon>
  );
}

 
function Header() {
  let naviris = useNavigate()
    const route_ris = () =>{
        let path_ris = '/ris';
        naviris(path_ris);
    }
    let navilog = useNavigate()
    const route_log = () =>{
        let path_log = '/log';
        navilog(path_log);
    }
    let navihomelog = useNavigate()
    const route_homelog = () =>{
        let path_homelog = 'Hl';
        navihomelog(path_homelog);
    }
    let navihome = useNavigate()
    const route_home = () =>{
        let path_home = '';
        navihome(path_home);
    }



    
  return (
    <Box sx={{ flexGrow: 1 }}>
    <AppBar position="static">
      <Toolbar>
        <IconButton
         size="large"
         edge="start"
         color="inherit"
         aria-label="menu"
         sx={{ mr: -90 }}
        >
          <HomeIcon sx={{ fontSize: 40 , color: yellow[500] }}onClick={route_homelog} />
        </IconButton>
        <Typography variant="h4" component="div" sx={{ flexGrow: 1  , color: yellow[500] }} >
          Travel the World
        </Typography>
        <Button color="inherit"onClick={route_ris} sx={{ color: yellow[500] }}> Register</Button>
        <Button color="inherit"onClick={route_log} sx={{ color: yellow[500] }}> Login</Button>
        <Button color="inherit"onClick={route_home} sx={{ color: yellow[500] }}> Logout</Button>
        
      </Toolbar>
    </AppBar>
  </Box>
);
}
export default Header;

