import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { Container } from "@mui/material";
import {useNavigate} from "react-router-dom";
import { yellow } from '@mui/material/colors';
import PageviewIcon from '@mui/icons-material/Pageview';


const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function RecipeReviewCard() {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  let navilog = useNavigate()
  const route_log = () =>{
      let path_log = '/risT';
      navilog(path_log);
  }

  return (
    
    <Container maxWidth='md'> 
    <br /><br />
    <Card sx={{ maxWidth: 1000 }} onClick={route_log}>   
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: yellow [500] }} aria-label="recipe">
            <PageviewIcon />
          </Avatar>
        }
        title="Japan / Okinawa"
      />
      <CardMedia
        component="img"
        height="400"
        image="https://digital.ihg.com/is/image/ihg/intercontinental---ana-okinawa-6162581297-4x3"
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
        Okinawa is the southernmost prefecture of Japan. It consists of hundreds of Ryukyu islands arranged in a line for more than a thousand kilometers. 
        until it is known as the Hawaii of Japan The largest island is Okinawa, with Naha as its capital. In the past, Okinawa was the center of the prosperous Ryukyu Kingdom. 
        And is an important port city In the old days, the Ryukyu Kingdom was a tribute to the Ming Dynasty of the Chinese Empire. therefore received various cultures from China, 
        both in terms of language style of architecture appliances and food.
        </Typography>
       
      </CardContent>
      <CardActions disableSpacing>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
      </Collapse>
    </Card>
    <br /><br />
    <Card sx={{ maxWidth: 1000 }}onClick={route_log}>   
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: yellow [500] }} aria-label="recipe">
            <PageviewIcon />
          </Avatar>
        }
        title="Japan / Tokyo"
      />

      <CardMedia
        component="img"
        height="400"
        image="https://cms.finnair.com/resource/blob/720698/0511bb2bc91953d4ef3506d00933819b/tokyo-main-data.jpg"
        
       />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
        Tokyo Metropolitan Japan's capital city attractions There are not many tourists who want to come to the center 
        of this Land of the Rising Sun. This is a place that can be said that you should not miss it if you have a chance to visit.
        Because it is a source of complete satisfaction for foodies, shoppers and culture lovers.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
      </Collapse>
    </Card>
    <br /><br />
    <Card sx={{ maxWidth: 1000 }}onClick={route_log}>   
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: yellow [500] }} aria-label="recipe">
             <PageviewIcon />
          </Avatar>
        }
        title="Switzerland / Zurich"
      />
      <CardMedia
        component="img"
        height="400"
        image="https://cdn.britannica.com/44/102444-050-7260C54D/Zurich-Switzerland.jpg"
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
        Zurich is one of Switzerland's major tourist destinations. 
        There are beautiful landscapes surrounded by mountains There are rivers and lakes Including both 
        historical and cultural landmarks such as old churches and the most famous art museums in Europe.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
      </Collapse>
    </Card>
    <br /><br />
    <Card sx={{ maxWidth: 1000 }}onClick={route_log}>   
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: yellow [500] }} aria-label="recipe">
             <PageviewIcon />
          </Avatar>
        }
        title="England / London"
      />
      <CardMedia
        component="img"
        height="400"
        image="https://www.syracuse.edu/images/cY6KOgf0WxbGixUTeTUUC62ZfVA=/3943/width-1100/london-large-image_08-10-202115-20-48.jpg"
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
        London is England's diverse capital city. Here, there are all kinds of interesting places to visit and are characterized 
        by beautiful art and amazing historical stories that make everyone want to visit once.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
      </Collapse>
    </Card>
    <br /><br />
    <Card sx={{ maxWidth: 1000 }}onClick={route_log}>   
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor:yellow [500] }} aria-label="recipe">
             <PageviewIcon />
          </Avatar>
        }
        title="Spain / Arrecife"
      />
      <CardMedia
        component="img"
        height="400"
        image="https://upload.wikimedia.org/wikipedia/commons/b/b6/Arrecife%2C_Lanzarote_2011.JPG"
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
        Arrecife is a port town on the east coast, served by ferries to the other canarian islands and Europe as well mainland Africa. 
        The LZ1 road connects Arrecife to the northeast of the island, the LZ2 road connects the capital to the island's southwest, 
        and the LZ3 road serves as the city's beltway.The tallest building in Lanzarote is the Arrecife Gran Hotel, which is located on the seafront alongside the harbour.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
      </Collapse>
    </Card>
    <br /><br />
    <Card sx={{ maxWidth: 1000 }}onClick={route_log}>   
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: yellow [500] }} aria-label="recipe">
             <PageviewIcon />
          </Avatar>
        }
        title="Korea / Cheongju"
      />
      <CardMedia
        component="img"
        height="400"
        image="https://images.kiwi.com/photos/600x330/cheongju_kr.jpg"
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
        Cheongju has been an important city of the province since ancient times. The provincial administrative center was moved 
        from Chungju to Cheongju in 1908. In 1946 Cheongju and Cheongwon District were separated, 
        and in 1949. Cheongju was upgraded to Cheongju. It was later divided into two branch offices,
         which were established in July 1989 and were upgraded to Sangdang District and Heungdeok District.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
      </Collapse>
    </Card>
    <br /><br />
    </Container>
    
    
  );
}