
import {useState} from "react"
import Button from '@mui/material/Button';
import Grid  from "@mui/material/Grid";
import { Box, Container, Typography } from "@mui/material";
import {useNavigate} from "react-router-dom";
import TextField from '@mui/material/TextField';
import Avatar from '@mui/material/Avatar';
import Stack from '@mui/material/Stack';
import { blue, deepOrange, yellow } from '@mui/material/colors';

function LoginPage (){

    const [user , setUser ] =useState("");
    const [password , setPassword ] =useState("");
    let navihome = useNavigate()
    const route_home = () =>{
        let path_home = '/Hl';
        navihome(path_home);
    }

    return(
    <Container maxWidth='lg'> 
        <Grid container spacing={2} sx={ { marginTop :"10px" } }>
            
  
        <Container align = "center"> 
                <br />
                <br />
                <Avatar  src="/broken-image.jpg" sx={{ width: 80, height: 80 ,color: yellow[500],bgcolor: blue[500]}}  />
                <br />
                <br />

                </Container>   
            
            
            <Grid item xs={12}>
                
               <Box  component="form" sx={{ '& .MuiTextField-root': { m: 1, width: '25ch' },}}
                noValidate
                autoComplete="off"
                >
                  <TextField 
                id="outlined-basic"
                label="Username"
                defaultValue={user}
                />
                <br />
                <br />
                 <TextField
                id="outlined-password-input"
                label="Password"
                defaultValue={password}
                type="password"
                autoComplete="current-password"
                />
                <br />
                <br />

                <Button variant="contained"  sx={{ color: yellow[500] }} onClick={route_home}> enter </Button>
                </Box>
            
            </Grid>
            
         </Grid> 
    </Container>
);   
    }

export default LoginPage;
