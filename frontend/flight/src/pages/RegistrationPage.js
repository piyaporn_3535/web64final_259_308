import * as React from 'react';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import { Container } from "@mui/material"; 

import Table from '@mui/material/Table';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Link } from 'react-router-dom';
import {useNavigate} from "react-router-dom";



export default function RegisterPage() {
    let navisuc = useNavigate()
const route_suc = () =>{
    let path_suc = '/suc';
    navisuc(path_suc);
}
  return (
    <Container maxWidth='md'> <br />
    <Stack component="form" noValidate spacing={3}>
      <TextField
        id="date"
        label="Date"
        type="date"s
        defaultValue="2017-05-24"
        sx={{ width: 220 }}
        InputLabelProps={{
          shrink: true,
        }}
      />
    </Stack>
    <br/>
    
    <TableContainer component={Paper} >
      <Table sx={{ minWidth: 650 }} aria-label="simple table" >
        <TableHead >
        
          <TableRow>
           
            <TableCell align="center" >ช่วงเวลาในการเดินทาง </TableCell>
            <TableCell align="center">ระยะเวลาในการเดินทาง</TableCell>
       
          </TableRow>
        </TableHead>
      </Table>
    </TableContainer>
  
  
    <TableContainer component={Paper}  onClick={route_suc}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            
            <TableCell align="center">08.05 - 22.40 น. </TableCell>
            <TableCell align="center">14 ชม. 35 นาที</TableCell>
       
          </TableRow>
        </TableHead>
      </Table>
    </TableContainer>

    <TableContainer component={Paper} onClick={route_suc}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            
            <TableCell align="center">23.15 - 07.30 น. </TableCell>
            <TableCell align="center">08 ชม. 15 นาที</TableCell>
       
          </TableRow>
        </TableHead>
      </Table>
    </TableContainer>

    <TableContainer component={Paper} onClick={route_suc} >
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            
            <TableCell align="center">23.30 - 06.15 น. </TableCell>
            <TableCell align="center">06 ชม. 45 นาที</TableCell>
       
          </TableRow>
        </TableHead>
      </Table>
    </TableContainer>
    <TableContainer component={Paper} onClick={route_suc}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            
            <TableCell align="center">10.05 - 17.30 น. </TableCell>
            <TableCell align="center">07 ชม. 25 นาที</TableCell>
       
          </TableRow>
        </TableHead>
      </Table>
    </TableContainer>
    <TableContainer component={Paper} onClick={route_suc}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            
            <TableCell align="center">20.25 - 13.05 น. </TableCell>
            <TableCell align="center">16 ชม. 40 นาที</TableCell>
       
          </TableRow>
        </TableHead>
      </Table>
    </TableContainer>
    <TableContainer component={Paper} onClick={route_suc}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            
            <TableCell align="center">23.50 - 16.30 น. </TableCell>
            <TableCell align="center">16 ชม. 40 นาที;</TableCell>
       
          </TableRow>
        </TableHead>
      </Table>
    </TableContainer>
    <br />
    <br />

    </Container>
  );
}