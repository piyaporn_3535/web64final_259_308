import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import { Container } from "@mui/material"; 
import {useNavigate} from "react-router-dom";

export default function SuccessPage() {
    let navihome = useNavigate()
    const route_home = () =>{
        let path_home = '/Hl';
        navihome(path_home);
    }

  return (
    <Container maxWidth='xs' onClick={route_home}> <br />
    <Card sx={{ maxWidth: 1000 }}>
      <CardActionArea>
        <CardMedia
          component="img"
          height="400"
          weight="600"
          image="https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Antu_task-complete.svg/1200px-Antu_task-complete.svg.png"
          
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
          Be completed
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
    </Container>
  );
}