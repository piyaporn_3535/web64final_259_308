
import {useState} from "react"
import {Link} from "react-router-dom";
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Grid  from "@mui/material/Grid";
import { Box, Container, Typography } from "@mui/material";
import {useNavigate} from "react-router-dom";
import Avatar from '@mui/material/Avatar';
import { yellow , blue } from "@mui/material/colors";
import AssignmentIcon from '@mui/icons-material/Assignment';


function RegisterPage(){

    const [name , setName ] =useState("");
    const [surname , setSurname ] =useState("");
    const [user , setUser ] =useState("");
    const [password , setPassword ] =useState("");

    let navilog = useNavigate()
    const route_log = () =>{
        let path_log = '/log';
        navilog(path_log);
    }
   


    return(
        
    <Container maxWidth='lg'> 
        <Grid container spacing={2} sx={ { marginTop :"10px" } }>
        <Container align = "center"> 
                <br />
                <br />
                <Avatar  variant="rounded" sx={{ width: 80, height: 80 ,color: yellow[500],bgcolor: blue[500]}}>
                    <AssignmentIcon sx={{ width: 80, height: 80 }} />
                </Avatar>
                <br />
                <br />

                </Container>   
            
            <Grid item xs={12}>
           
            <Box  component="form" sx={{ '& .MuiTextField-root': { m: 1, width: '25ch' },}}
                noValidate
                autoComplete="off"
                >

                <TextField
                id="outlined-basic"
                label="Name"
                defaultValue={name}
                /> 
                 <TextField
                id="outlined-basic"
                label="Surname"
                defaultValue={surname}
                />
                <br />
                <br />
                 <TextField 
                id="outlined-basic"
                label="Username"
                defaultValue={user}
                />
                 <TextField
                id="outlined-password-input"
                label="Password"
                defaultValue={password}
                type="password"
                autoComplete="current-password"
                />
                <br />
                <br />
                <Button variant="contained"  sx={{ color: yellow[500] }} onClick={route_log}> enter </Button>
                </Box>
            </Grid>
         </Grid> 
    </Container>
);   
    }

export default RegisterPage;