const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql');

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'root',
    database : 'Flight_Booking'
});


connection.connect();

const express = require('express');
const app = express()
const port = 4000

/*Middleware for Authenticating User Token */

function authenticateToken(req ,res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) =>{
        if (err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}

/*API for Registering a new Tourist */
app.post("/register_tourist", (req,res) =>{
    let tourist_name  = req.query.tourist_name
    let tourist_surname = req.query.tourist_surname
    let tourist_username = req.query.tourist_username
    let tourist_password = req.query.tourist_password
    
    bcrypt.hash(tourist_password, SALT_ROUNDS, (err, hash) => {
        let query = `INSERT INTO  Tourist 
                 (TouristName, TouristSurname, Username, Password)
                 VALUES ('${tourist_name}','${tourist_surname}',
                         '${tourist_username}', '${hash}')`
        
        console.log(query)

        connection.query( query,(err, rows) =>{
            console.log(err)
                if (err){
                    res.json({
                        "status" : "400",
                        "message" : "Error inserting data into db"
                        })
                }else {
                    res.json ({
                            "status" : "200",
                            "message" : "Adding new user succesful"
                    })
            }
        });

    })   
 })

 /*API for Registering to a new Tourist Event */
app.post("/register_booking", authenticateToken, (req, res)  =>  {
   
    let tourist_id = req.query.tourist_id
    let booking_id = req.query.booking_id
    let quantity = req.query.quantity

    let query = `INSERT INTO Registration
                (TouristID, BookingID,Quantity)
                VALUES (${tourist_id},
                        ${booking_id},
                        ${quantity}
                        )`

    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if (err) {
            res.json({
                "status" : "400",
                "message" : "Error inserting data into db"
                   })
        }else {
            res.json({
                "status" : "200",
                "message" : "Adding booking succesful"
            })
        }
    });
})

/* API for Processing Tourist Authorization */
app.post("/login", (req, res) =>{
    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM Tourist WHERE Username = '${username}'`
    connection.query( query, (err,rows) =>{
                if (err) {
                    console.log(err)
                    res.json({
                        "status" : "400",
                        "message" : "Error querying from booking db"
                        })
                }else {
                    let db_password = rows[0].Password
                    bcrypt.compare(user_password, db_password, (err, result) =>{
                        if (result){
                          let payload = {
                              "username" : rows[0].Username,
                              "user_id" : rows[0].TouristID,
                              
                          }
                          console.log(payload)
                          let token = jwt.sign(payload, TOKEN_SECRET, {expiresIn : '1d'})
                          res.send(token)
                        }else { res.send ("Invalid username / password")}
                    })

                } 
    })
})

/* CRUD  Operation for Booking Table */
app.get("/list_booking", (req, res) => {
    query = "SELECT * from Booking";
connection.query( query, (err, rows) => {
        if (err) {
                res.json({
                            "status" : "400",
                            "message" : "Error querying from running db"
                        })
        }else {
            res.json(rows)
        }
    });
})


app.post("/update_booking",(req,res) =>{
    let booking_id = req.query.booking_id
    let booking_name  = req.query.booking_name
    let booking_location = req.query.booking_location
    let booking_time = req.query.booking_time

    
    let query = `UPDATE Booking SET 
                 BookingName = '${booking_name}',
                 BookingLocation = '${booking_location}',
                 BookingTime = '${booking_time}'
                 WHERE BookingID  = ${booking_id}`

    console.log(query)

    connection.query( query,(err, rows) =>{
        if (err) {
            console.log(err)
            res.json({
                   "status" : "400",
                   "message" : "Error updating record"
            })
        }else {
            res.json ({
                    "status" : "200",
                    "message" : "Updating booking succesful"
            })
        }
    });
})

app.post("/update_tourist",(req,res) =>{
    let tourist_id = req.query.tourist_id
    let tourist_name  = req.query.tourist_name
    let tourist_surname = req.query.tourist_surname
    let username = req.query.username
    let password = req.query.password
        
    let query = `UPDATE Tourist SET 
                 TouristName = '${tourist_name}',
                 TouristSurname = '${tourist_surname}',
                 Username = '${username}',
                 Password = '${password}'
                 WHERE TouristID  = ${tourist_id}`

    console.log(query)

    connection.query( query,(err, rows) =>{
        if (err) {
            console.log(err)
            res.json({
                   "status" : "400",
                   "message" : "Error updating record"
            })
        }else {
            res.json ({
                    "status" : "200",
                    "message" : "Updating music succesful"
            })
        }
    });
})


app.post("/add_booking", (req, res)  =>  {

    
    let booking_name = req.query.booking_name
    let booking_location = req.query.booking_location
    let booking_time = req.query.booking_time

    let query = `INSERT INTO Booking
                (BookingName,BookingLocation,BookingTime)
                VALUES ('${booking_name}','${booking_location}','${booking_time}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                "status" : "400",
                "message" : "Error inserting data into db"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Adding booking succesful"
            })
        }
    });
    
})

app.post("/add_tourist", (req, res)  =>  {

    
    let tourist_name = req.query.tourist_name
    let tourist_surname = req.query.tourist_surname
    let username = req.query.username
    let password = req.query.password

    

    let query = `INSERT INTO Tourist
                (TouristName,TouristSurname,Username,Password)
                VALUES ('${tourist_name}','${tourist_surname}','${username}','${password}' )`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                "status" : "400",
                "message" : "Error inserting data into db"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Adding event succesful"
            })
        }
    });
    
})

app.post("/delete_booking", (req, res)  =>  {

    let booking_id = req.query.booking_id
   

    let query = `DELETE FROM Booking WHERE BookingID=${booking_id}`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status" : "400",
                "message" : "Error deleting record"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Deleting record succesful"
            })
        }
    });

})


app.post("/delete_tourist", (req, res)  =>  {

    let tourist_id = req.query.tourist_id
   

    let query = `DELETE FROM Tourist WHERE TouristID=${tourist_id}`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status" : "400",
                "message" : "Error deleting record"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Deleting record succesful"
            })
        }
    });

})


app.listen(port, () => {
    console.log(`Now starting Running System Backend ${port} `)
})
